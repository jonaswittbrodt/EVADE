name = "TRSM";

Phi = {{0}, {1/ Sqrt[2] (vhhr0)}};
S = 1/Sqrt[2] (vhsr0);
X = 1/Sqrt[2] (vhxr0);
V = ComplexExpand[
    muHsq ConjugateTranspose[Phi].Phi + LH (ConjugateTranspose[Phi].Phi)^2 
    + muSsq S ^2 + LS S^4 
    + muXsq X^2 + LX X^4 
    + LHS ConjugateTranspose[Phi].Phi S^2 
    + LHX ConjugateTranspose[Phi].Phi X^2
    + LSX S^2 X^2][[1,1]];

fields = {vhhr0, vhsr0, vhxr0};
parameters = {muHsq, muSsq, muXsq, LH, LS, LX, LHS, LHX, LSX, vSM, vs, vx};
ewvacuum = {vhhr0 -> vSM + vhhr0, vhsr0 -> vs + vhsr0, vhxr0 -> vx + vhxr0};

generateTRSM[] := generateModel[name, V, fields, parameters, ewvacuum];
