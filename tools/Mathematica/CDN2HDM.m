name = "CDN2HDM";

Phi1 = {{1/Sqrt[2] (vh1rp + I vh1ip)}, {1/Sqrt[2] (vh1r0 + I*vh1i0)}};
Phi2 = {{1/Sqrt[2] (vh2rp + I vh2ip)}, {1/Sqrt[2] (vh2r0 + I*vh2i0)}};
PhiS = vhsr0;
V = ComplexExpand[m11sq ConjugateTranspose[Phi1].Phi1
		+ m22sq ConjugateTranspose[Phi2].Phi2
		+ 1/2 mssq PhiS^2
		+ L1/2 (ConjugateTranspose[Phi1].Phi1)^2
		+ L2/2 (ConjugateTranspose[Phi2].Phi2)^2
		+ L3 ConjugateTranspose[Phi1].Phi1*ConjugateTranspose[Phi2].Phi2
		+ L4 ConjugateTranspose[Phi1].Phi2*ConjugateTranspose[Phi2].Phi1
		+ L5/2 ((ConjugateTranspose[Phi1].Phi2)^2 + (ConjugateTranspose[Phi2].Phi1)^2)
		+ L6/8 PhiS^4
		+ L7/2 (ConjugateTranspose[Phi1].Phi1) PhiS^2
		+ L8/2 (ConjugateTranspose[Phi2].Phi2) PhiS^2
    + (Tr + I Ti) PhiS ConjugateTranspose[Phi1].Phi2 + (Tr - I Ti) PhiS ConjugateTranspose[Phi2].Phi1 ][[1,1]];

fields = {vh1r0, vh1i0, vh2r0, vh2i0, vhsr0, vh1rp, vh1ip, vh2rp, vh2ip};
parameters = {vSM, L1, L2, L3, L4, L5, L6, L7, L8, Tr, Ti, m11sq, m22sq, mssq};
ewvacuum = {vh1r0 -> vSM + vh1r0};

generateCDN2HDM[]:= generateModel[name,V,fields,parameters,ewvacuum]
