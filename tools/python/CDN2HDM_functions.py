import numpy as np


def fromScannerS(input, output):
    import pandas as pd

    df = pd.read_table(input, index_col=False)
    list(df)
    df.rename(columns={'v': 'vSM'}, inplace=True)
    df.to_csv(output, sep='\t')


def categorize(df, kind, tol=1e-3):
    import matplotlib.pyplot as plt

    def vev(name):
        return np.abs(df[kind + name]) > tol
    stable = (df.fast_B == -2) | (df.fast_B == -3) | (df.fast_B == -4)
    sm = (vev('vh1r0'))
    idm = (vev('vh2r0'))
    s = vev('vhsr0')
    cp = vev('vh2i0')
    cb = vev('vh2rp')
    return [
        [sm & np.invert(s | cp | cb | stable), {
            "label": r"$h_{125}$ VEV", 'c': 'C0'}],
        [idm & np.invert(sm | s | cp | cb | stable), {
            "label": r"real $H_2$ VEV", 'c': 'C1'}],
        [s & np.invert(sm | idm | cp | cb | stable), {
            "label": r"$S$ VEV", 'c': 'C2'}],
        [sm & idm & cp & np.invert(s | cb | stable), {
            "label": r"CP-breaking $H$ VEVs", 'c': 'C3'}],
        [sm & idm & cp & s & np.invert(cb | stable), {
            "label": r"CP-breaking $H, S$ VEVs", 'c': 'C4'}],
        [sm & idm & cb & np.invert(s | cp | stable), {
            "label": r"$e$-breaking $H$ VEVs", 'c': 'C5'}],
        [sm & idm & cb & s & np.invert(cp | stable), {"label": r"$e$-breaking $H, S$ VEVs", 'c': 'C6'}]]
