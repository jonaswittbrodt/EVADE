#include "EVADE/Fieldspace/Point.hpp"

#include "EVADE/Utilities/Approx.hpp"
#include "EVADE/Utilities/Utilities.hpp"
#include <iostream>

namespace EVADE {

namespace Fieldspace {
using Utilities::Approx;

Point::Point(std::vector<double> fields, double V)
    : fields_{std::move(fields)}, V_{V} {}

bool Point::Degenerate(const Point &comp) const {
  return Approx(V_).Rel(rtolV).Abs(atolV) == comp.V_;
}

bool Point::EqualFields(const Point &comp) const {
  if (fields_.size() != comp.fields_.size())
    throw(std::runtime_error(
        "Cannot compare Fieldspace::Points of different sizes (" +
        std::to_string(fields_.size()) + " vs " +
        std::to_string(comp.fields_.size()) + ")!"));

  auto fieldEqual = [](double a, double b) -> bool {
    return a == Approx(b).Rel(rtolFields).Abs(atolFields);
  };

  return std::equal(fields_.begin(), fields_.end(), comp.fields_.begin(),
                    fieldEqual);
}

const std::vector<double> &Point::Fields() const { return fields_; }

double Point::V() const { return V_; }

bool operator==(const Point &a, const Point &b) {
  return a.Degenerate(b) && a.EqualFields(b);
}

bool operator<(const Point &a, const Point &b) {
  if (a == b)
    return false;

  if (!a.Degenerate(b))
    return a.V() < b.V();

  auto fieldComp = [](double aa, double bb) -> bool {
    return aa > Approx(bb).Rel(Point::rtolFields).Abs(Point::atolFields);
  };
  return std::lexicographical_compare(a.fields_.begin(), a.fields_.end(),
                                      b.fields_.begin(), b.fields_.end(),
                                      fieldComp);
}

std::string Fieldspace::Point::ToString(IO::PrintLabels printLabels,
                                        int precision,
                                        const std::string &separator) const {
  std::stringstream ss;

  ss.precision(precision);
  if (printLabels == IO::PrintLabels::yes) {
    ss << "V: ";
  }
  ss << V_ << separator;
  if (printLabels == IO::PrintLabels::yes) {
    ss << "fields: {";
  }
  for (size_t i = 0; i != fields_.size(); ++i) {
    if (i != 0) {
      ss << separator;
    }
    ss << fields_[i];
  }
  if (printLabels == IO::PrintLabels::yes) {
    ss << "}";
  }
  return ss.str();
}

std::ostream &operator<<(std::ostream &os, const Fieldspace::Point &obj) {
  os << obj.ToString();
  return os;
}

} // namespace Fieldspace
} // namespace EVADE
