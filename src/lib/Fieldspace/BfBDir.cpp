#include "EVADE/Fieldspace/BfBDir.hpp"

#include "EVADE/Utilities/Approx.hpp"

namespace EVADE {
namespace Fieldspace {

BfBDir::BfBDir(double lambda, Direction direction)
    : Direction{std::move(direction)}, lam{lambda} {}

bool operator==(const BfBDir &a, const BfBDir &b) {
  return a.lam == Utilities::Approx(b.lam)
                      .Abs(BfBDir::atolLam)
                      .Rel(BfBDir::rtolLam) &&
         static_cast<Direction>(a) == static_cast<Direction>(b);
}

bool operator<(const BfBDir &a, const BfBDir &b) {
  if (a == b)
    return false;
  if (a.lam ==
      Utilities::Approx(b.lam).Abs(BfBDir::atolLam).Rel(BfBDir::rtolLam))
    return static_cast<Direction>(a) < static_cast<Direction>(b);
  return a.lam < b.lam;
}

} // namespace Fieldspace
} // namespace EVADE
