#include "EVADE/Utilities/Approx.hpp"

#include <algorithm>
#include <cmath>
#include <limits>

EVADE::Utilities::Approx::Approx(double value)
    : relTol_(std::numeric_limits<float>::epsilon() * 100), absTol_(0.0),
      value_(value) {}

bool EVADE::Utilities::Approx::EqualityImplementation(double other) const {
  // std::abs(value_ - other) <= absTol_ in an inf safe way
  if ((value_ + absTol_ >= other) && (other + absTol_ >= value_)) {
    return true;
  }

  // all infs in values are handled by the absolute comparison
  if (std::isinf(value_) || std::isinf(other)) {
    return false;
  }

  if (std::abs(value_ - other) <=
      std::max(std::abs(value_), std::abs(other)) * relTol_) {
    return true;
  }
  return false;
}

EVADE::Utilities::Approx &EVADE::Utilities::Approx::Rel(double relTol) {
  // a relative tolerance of 2.0 contains the whole range (both signs)
  relTol_ = std::min(std::abs(relTol), 2.);
  return *this;
}

EVADE::Utilities::Approx &EVADE::Utilities::Approx::Abs(double absTol) {
  absTol_ = std::abs(absTol);
  return *this;
}

bool EVADE::Utilities::operator==(double lhs,
                                  const EVADE::Utilities::Approx &rhs) {
  return rhs.EqualityImplementation(lhs);
}

bool EVADE::Utilities::operator<(double lhs,
                                 const EVADE::Utilities::Approx &rhs) {
  return lhs < rhs.value_ && lhs != rhs;
}

bool EVADE::Utilities::operator<(const EVADE::Utilities::Approx &lhs,
                                 double rhs) {
  return lhs.value_ < rhs && lhs != rhs;
}
