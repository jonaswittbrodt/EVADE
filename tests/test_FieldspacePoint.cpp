#include "EVADE/Fieldspace/Point.hpp"
#include "catch.hpp"

#include <cmath>

using namespace EVADE;

TEST_CASE("FieldspacePoints basic", "[unit][fspoint]") {
  Fieldspace::Point test({10., 20., 30., 40., 50.}, -1000);

  SECTION("getting values") {
    REQUIRE(test.Fields().size() == 5);
    REQUIRE(test.V() == CatchApprox(-1000.));
    REQUIRE(test.Fields()[0] == CatchApprox(10.));
  }
  SECTION("iterating") {
    std::size_t i = 0;
    for (auto x : test.Fields()) {
      REQUIRE(std::isnormal(x));
      ++i;
    }
    REQUIRE(i == test.Fields().size());
  }
}

TEST_CASE("Comparing FieldspacePoints by potential values",
          "[unit][fspoint][approx]") {
  std::vector<double> testfields = {10., 20., 30., 40., 50.};
  Fieldspace::Point test(testfields, -1000);

  SECTION("equal FPs are equal") {
    Fieldspace::Point test2(test);

    REQUIRE(test == test2);
  }
  SECTION("slightly deeper FPs are still equal") {
    Fieldspace::Point test2(testfields, -1010);

    REQUIRE(test == test2);
  }
  SECTION("way deeper FPs are deeper") {
    Fieldspace::Point test2(testfields, -2e8);

    REQUIRE(test > test2);
    REQUIRE_FALSE(test == test2);
  }
}

TEST_CASE("Comparing FieldspacePoints by field values",
          "[unit][fspoint][approx]") {
  double testpot = -1000;
  Fieldspace::Point test({10., 20., 30., 40., 50.}, testpot);

  SECTION("sign flip ordering") {
    Fieldspace::Point test2({-10., 20., 30., 40., 50.}, testpot);

    REQUIRE(test < test2);
    REQUIRE_FALSE(test == test2);
  }
  SECTION("slightly different field value ignored") {
    Fieldspace::Point test2({9.9, 20., 30., 40., 50.}, testpot);

    REQUIRE(test == test2);
  }
  SECTION("also works for elements other than the first") {
    Fieldspace::Point test2({10., 20., 30., 40., 0.}, testpot);

    REQUIRE(test < test2);
    REQUIRE_FALSE(test == test2);
  }
  SECTION("lexicographical ordering, only first difference counts") {
    Fieldspace::Point test2({10., 30., -20, -20, -20}, testpot);
    Fieldspace::Point test3({10., 20., -20, -20, -20}, testpot);

    REQUIRE_FALSE(test == test2);
    REQUIRE_FALSE(test3 == test);
    REQUIRE_FALSE(test3 == test2);

    REQUIRE(test2 < test);
    REQUIRE(test < test3);
    REQUIRE(test2 < test3);
  }
}
