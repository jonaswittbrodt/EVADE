#include "ParameterReader.hpp"

#include "EVADE/config.h"
#include "catch.hpp"

TEST_CASE("Reading test file", "[unit][reader]") {
  SECTION("Valid file with nparam") {
    auto reader =
        ParameterReader(TESTDIR + std::string("/testdata_simple.in"), 16);

    std::vector<double> param;
    std::string id;
    while (reader.GetPoint(id, param)) {
      REQUIRE(param.size() == 16);
    }
    REQUIRE(id == "9");
    REQUIRE(param.size() == 16);
  }

  SECTION("Valid file with columns") {
    std::vector<size_t> columns{0, 1, 6, 3, 7, 9};
    auto reader =
        ParameterReader(TESTDIR + std::string("/testdata_simple.in"), columns);

    std::vector<double> param;
    std::string id;
    while (reader.GetPoint(id, param)) {
      REQUIRE(param.size() == columns.size());
    }
    REQUIRE(id == "9");
    REQUIRE(param.size() == columns.size());
  }
  SECTION("Valid file with names") {
    std::vector<std::string> colNames{"HMIX_1", "TU_3_3", "GAUGE_1"};
    auto reader =
        ParameterReader(TESTDIR + std::string("/testdata_names.in"), colNames);

    std::vector<double> param;
    std::string id;
    while (reader.GetPoint(id, param)) {
      REQUIRE(param.size() == colNames.size());
    }
    REQUIRE(id == "9");
    REQUIRE(param.size() == colNames.size());
  }

  SECTION("No file found") {
    REQUIRE_THROWS_WITH(
        ParameterReader(TESTDIR + std::string("/NOtestdata.in"), 16),
        Catch::Matchers::Contains("ParameterReader cannot open"));
  }

  SECTION("Invalid file for nparam") {
    auto reader =
        ParameterReader(TESTDIR + std::string("/testdata_simple.in"), 100);

    std::vector<double> param;
    std::string id;
    REQUIRE_THROWS_AS(reader.GetPoint(id, param), std::out_of_range);
  }

  SECTION("Invalid file for columns") {
    std::vector<size_t> columns{0, 1, 6, 3, 100, 9};

    auto reader =
        ParameterReader(TESTDIR + std::string("/testdata_simple.in"), columns);

    std::vector<double> param;
    std::string id;
    REQUIRE_THROWS_AS(reader.GetPoint(id, param), std::out_of_range);
  }
  SECTION("Inalid file with names") {
    std::vector<std::string> colNames{"HMIX_1", "TU_3_3", "GAUGE_1",
                                      "NOT_A_PARAMETER"};
    REQUIRE_THROWS_WITH(
        ParameterReader(TESTDIR + std::string("/testdata_simple.in"), colNames),
        Catch::Matchers::Contains("not found in input file"));
    REQUIRE_THROWS_WITH(
        ParameterReader(TESTDIR + std::string("/testdata_names.in"), colNames),
        Catch::Matchers::Contains("not found in input file"));
  }
}
