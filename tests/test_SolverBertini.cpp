#include "EVADE/Models/MSSM.hpp"
#include "EVADE/ScalingPolicies/Generic.hpp"
#include "EVADE/ScalingPolicies/Unscaled.hpp"
#include "EVADE/SolverPolicies/Bertini.hpp"
#include "EVADE/StationarityConditions.hpp"
#include "EVADE/Utilities/Polynomial.hpp"
#include "catch.hpp"

#include <algorithm>
#include <cmath>

using namespace EVADE;

TEST_CASE("Bertini working", "[unit][bertini]") {
  Solver::Bertini solver;
  Utilities::Polynomial eq1;

  eq1[{2, 0}] = 1.;
  eq1[{1, 1}] = -2.;
  eq1[{0, 2}] = -1.;
  eq1[{0, 0}] = 4.;

  Utilities::Polynomial eq2;

  eq2[{2, 0}] = 1.;
  eq2[{1, 1}] = -3.;
  eq2[{0, 2}] = 2.;

  std::vector<std::string> varnames{"x1", "x2"};
  auto result = solver.Call({eq1, eq2}, varnames);

  std::sort(result.begin(), result.end());

  REQUIRE(result[0][0] == CatchApprox(-4.));
  REQUIRE(result[0][1] == CatchApprox(-2.));

  REQUIRE(result[1][0] == CatchApprox(-sqrt(2.)));
  REQUIRE(result[1][1] == CatchApprox(-sqrt(2.)));

  REQUIRE(result[2][0] == CatchApprox(sqrt(2.)));
  REQUIRE(result[2][1] == CatchApprox(sqrt(2.)));

  REQUIRE(result[3][0] == CatchApprox(4.));
  REQUIRE(result[3][1] == CatchApprox(2.));
}

TEST_CASE("MSSM statconds scaling bertini",
          "[unit][mssm][statconds][bertini][gscaling]") {
  auto statconds =
      StationarityConditions<Models::MSSM, Scaling::Generic, Solver::Bertini>();

  std::vector<double> testpars{
      0.36151919899999996, // GAUGE_1
      0.63675145,          // GAUGE_2
      1.04060771,          // GAUGE_3
      -7155.0,             // HMIX_1
      246.21845810181637,  // HMIX_3
      863041.0,            // HMIX_4
      1.5232132235179132,  // HMIX_10
      0.8309101757104086,  // YU_3_3
      0.2319447148681335,  // YD_3_3
      0,                   // YE_3_3
      -50328892.06354008,  // MSOFT_21
      -51196116.93645992,  // MSOFT_22
      8743849.0,           // MSQ2_3_3
      8743849.0,           // MSU2_3_3
      8743849.0,           // MSD2_3_3
      8743849.0,           // MSL2_3_3
      8743849.0,           // MSE2_3_3
      3438.306307089671,   // TU_3_3
      1349.148564788819,   // TD_3_3
      0.,                  // TE_3_3
  };

  SECTION("Minimal field content") {
    auto result = statconds.SolveForFields(testpars, {"vhur0", "vhdr0"});
    size_t nsol = result.size();

    REQUIRE(nsol == 3);
    REQUIRE(std::find(
                result.begin(), result.end(),
                Fieldspace::Point({245.94, 0, 11.7114, 0, 0, 0, 0, 0, 0, 0, 0,
                                   0,      0, 0,       0, 0, 0, 0, 0, 0, 0, 0},
                                  -6.10209e7)) != result.end());
    REQUIRE(std::find(result.begin(), result.end(),
                      Fieldspace::Point({-245.94, 0, -11.7114, 0, 0, 0, 0, 0,
                                         0,       0, 0,        0, 0, 0, 0, 0,
                                         0,       0, 0,        0, 0, 0},
                                        -6.10209e7)) != result.end());
    REQUIRE(std::find(result.begin(), result.end(),
                      Fieldspace::Point({0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                        0)) != result.end());
  }

  SECTION("With squark vevs") {
    auto result = statconds.SolveForFields(
        testpars, {"vhur0", "vhdr0", "vulr3", "vurr3", "vdlr3", "vdrr3"});
    std::sort(result.begin(), result.end());
    REQUIRE(result.size() == 27);
    REQUIRE(result[16] ==
            Fieldspace::Point({245.94, 0, 11.7114, 0, 0, 0, 0, 0, 0, 0, 0,
                               0,      0, 0,       0, 0, 0, 0, 0, 0, 0, 0},
                              -6.10209e7));
    REQUIRE(result[17] ==
            Fieldspace::Point({-245.94, 0, -11.7114, 0, 0, 0, 0, 0, 0, 0, 0,
                               0,       0, 0,        0, 0, 0, 0, 0, 0, 0, 0},
                              -6.10209e7));
    REQUIRE(result[18] == Fieldspace::Point({0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                                            0));
    REQUIRE(result[0] ==
            Fieldspace::Point({36873.5, 0, 41169.1,  0, 0, 0, 0, 0, 0, 0, 0, 0,
                               25374.8, 0, -25962.2, 0, 0, 0, 0, 0, 0, 0},
                              -1.037934e+16));
    REQUIRE(result[1] ==
            Fieldspace::Point({36873.5,  0, 41169.1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                               -25374.8, 0, 25962.2, 0, 0, 0, 0, 0, 0, 0},
                              -1.037934e+16));
  }
}
