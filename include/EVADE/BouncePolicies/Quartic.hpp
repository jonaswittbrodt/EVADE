#pragma once
/** @file */
#include <vector>

namespace EVADE {
namespace Bounce {

/** @relates Quartic
 * This function calculates the bounce along a path given by a quartic
 * polynomial \f$ \mathrm{lam}\cdot x^4 - \mathrm{a}\cdot x^3 +
 * \mathrm{msq}\cdot x^2 \f$. The initial vacuum is at \f$x=0\f$ and the path
 * has to be *appropriately normalized*. If there is no wall in the given
 * direction this function returns 0. If the direction is unbounded or there is
 * no deeper minimum it returns the appropriate EVADE::BounceCode. For more information
 * see [hep-ph/9302321](https://arxiv.org/abs/hep-ph/9302321).
 * @param  msq quadratic coefficient
 * @param  a   cubic coefficient
 * @param  lam quartic coefficient
 * @return     the bounce along the path
 */
double QuarticBounce(double msq, double a, double lam);

/**
 * Template class to calculate the bounce for a quartic polynomial potential.
 * @tparam Model The model to use. This has to provide Msq, A and Lam member
 * functions taking a direction as an argument.
 */
template <class Model> class Quartic : public Model {
public:
  /**
   * Constructor which initializes the Model with the given parameters.
   * @param parameters the model parameters
   */
  Quartic(const std::vector<double> &parameters) : Model{parameters} {}

  /**
   * Takes a direction and calculates the QuarticBounce() in this direction
   * using the coefficients supplied by the Model.
   * @return the bounce
   */
  double operator()(const std::vector<double> &direction) const {
    return QuarticBounce(Model::Msq(direction), Model::A(direction),
                         Model::Lam(direction));
  }
};

} // namespace Bounce
} // namespace EVADE
