#pragma once
/** @file */
#include "EVADE/Utilities/TempDir.hpp"

#include <cstddef>
#include <stdexcept>
#include <string>
#include <vector>

namespace EVADE {

namespace Utilities {
class Polynomial;
}

namespace Solver {
/**
 * SolverPolicy interfacing with the HOM4PS2 homotopy solver. The interface has
 * to rely on files and is made thread-safe through the use of uniquely named
 * temporary folder (see TempDir). For this reason this is a move-only type.
 */
class Hom4ps2 {
public:
  /**
   * The homotopy types available in HOM4PS2.
   * Linear is usually faster for finding stationary points of quartic
   * polynomials.
   */
  enum class HomType { polyhedral = 1, linear = 2 };

  /**
   * Solves a system of polynomial equations using HOM4PS2. This function is the
   * required SolverPolicy interface. It takes a system of equations and names
   * for the variables (ordered as they are in the polynomial) and returns a
   * vector of solutions (where the fields are orderes as in varnames).
   * @param equations the system of equations to solve
   * @param varnames  the variable names corresponding to equations, **must not
   * contain any whitespace**
   * @return the solutions of the system ordered as varnames
   */
  std::vector<std::vector<double>>
  Call(const std::vector<Utilities::Polynomial> &equations,
       const std::vector<std::string> &varnames);

  /**
   * Can also be used to change the homotopy type from the default if needed.
   * @param  path    path where the temporary directory will be created
   * @param  homtype homotopy type
   */
  explicit Hom4ps2(const std::string &path = ".",
                   HomType homtype = HomType::linear);

private:
  enum class OnBlowup { throwEx, ignore };

  void WriteInput(const std::vector<Utilities::Polynomial> &equations,
                  const std::vector<std::string> &varnames);

  void Run();

  std::vector<std::vector<double>>
  ReadResult(const std::vector<std::string> &varnames,
             OnBlowup onBlowup = OnBlowup::throwEx);

  Utilities::TempDir folder_;
  const std::string command_;
};

/**
 * Exception class that gets thrown when HOM4PS2 notices a blowup.
 * This is caught within Solver::Hom4ps2 and triggers a rerun of the
 * calculation. Since blowups depend on the internal random numbers of HOM4PS2
 * they can be circumvented by retrying.
 */
class Hom4ps2Blowup : public std::runtime_error {
public:
  Hom4ps2Blowup(); //!<
};

/**
 * Reports a serious error in running HOM4PS2.
 * This exception leaves the solver since it usually requires manual
 * intervention. The what() output contains the location of the failing input
 * file as well as the verbatim command used to invoke HOM4PS2.
 */
class Hom4ps2RunError : public std::runtime_error {
public:
  /**
   * Constructs a Hom4ps2RunError.
   * @param exitStatus  HOM4PS2 exit status
   * @param folder      the Utilities::TempDir instance
   * @param callCommand the command used to call HOM4PS2
   */
  Hom4ps2RunError(int exitStatus, EVADE::Utilities::TempDir &folder,
                  const std::string &callCommand);
};

/**
 * Reports an error in reading the roots from the HOM4PS2 output.
 * This is usually triggered by numerical issues with judging whether a root is
 * real or complex. The what() output contains the location of the output file
 * as well as how many real and total roots were read.
 */
class Hom4ps2ReadError : public std::runtime_error {
public:
  /**
   * Constructs a Hom4ps2ReadError.
   * @param folder the Utilities::TempDir instance
   * @param nTot   the total number of roots read
   * @param nReal  the number of real roots read
   */
  Hom4ps2ReadError(EVADE::Utilities::TempDir &folder, size_t nTot,
                   size_t nReal);
};

} // namespace Solver
} // namespace EVADE
