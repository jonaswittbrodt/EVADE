#pragma once
/** @file */
#include <vector>

#include "EVADE/Fieldspace/Direction.hpp"
#include "EVADE/Fieldspace/Point.hpp"
#include "EVADE/Fieldspace/TunnellingDir.hpp"

namespace EVADE {

namespace Bounce {
template <class> class Quartic;
}

/**
 * Template class to evaluate the stability of a model parameter point by
 * calculating the tunneling rates.
 * @tparam Model The model in use. This class uses static members of Model.
 * @tparam OptimizationPolicy The optimizations to perform on the direction of
 * the bounce.
 */
template <class Model, template <class> class BouncePolicy = Bounce::Quartic>
class TunnellingCalculator {
  BouncePolicy<Model> bounce_;
  Fieldspace::Point initialVac_;

public:
  /**
   * Constructor which forwards the modeParams to the BouncePolicy and gets the
   * FieldspacePoint describing the initial vacuum from the model.
   * @param modelParams the model parameters
   */
  TunnellingCalculator(const std::vector<double> modelParams)
      : bounce_{modelParams},
        initialVac_{Model::InitialVacuum(modelParams),
                    Model::V(modelParams, Model::InitialVacuum(modelParams))} {}

  /**
   * Calculates the bounce according to BouncePolicy. Calculates the bounce
   * action from the initial vacuuum to the target point. If the argument is the
   * initial vacuum its bounce value is instead set to BounceCode::initial. If
   * the target point is degenerate with the initial point it is set to
   * BounceCode::initDegenerate. If it is not deeper it is set to
   * BounceCode::notDeeper.
   * @param  toPoint the target point
   * @return         the optimized bounce action
   */
  Fieldspace::TunnellingDir CalculateBounce(Fieldspace::Point toPoint) {
    using Fieldspace::TunnellingDir;
    Fieldspace::Direction dir{initialVac_, std::move(toPoint)};
    if (dir.Target() == dir.Origin())
      return TunnellingDir{BounceCode::initial, std::move(dir)};
    if (dir.Target().Degenerate(dir.Origin()))
      return TunnellingDir{BounceCode::initDegenerate, std::move(dir)};
    if (dir.Target().V() < dir.Origin().V()) {
      double B = bounce_(dir.Dir());
      return TunnellingDir{B, std::move(dir)};
    }
    return TunnellingDir{BounceCode::notDeeper, std::move(dir)};
  }
};

} // namespace EVADE
