#pragma once
/** @file */
#include <atomic>
#include <cstddef>
#include <string>
#include <vector>

namespace EVADE {
namespace Utilities {

/**
 * This class owns a temporary directory. A unique identifier based on the PID
 * and an (atomic) instance count is appended to the prefixPath as the directory
 * name. It provides interface for creating subfolders and symlinks in the
 * temporary path. At the end of this objects lifetime all created links as well
 * as any additional files specified for cleanup are deleted and all created
 * directories are removed (if empty). Since it owns the directory the type is
 * move-only. Consecutive slashes will be replaced by a single one in all paths
 * passed into this class.
 */
class TempDir {
public:
  /**
   * Constructs a temporary directory at prefixPath + unique ID.
   * Will create a directory tree if required.
   * @param  prefixPath prefix for the directory
   */
  explicit TempDir(const std::string &prefixPath);

  /**
   * Creates a subfolder in the TempDir
   * @param location relative path to the subfolder within the TempDir
   */
  void MakeSubfolder(const std::string &location);

  /**
   * Symlinks a target file into the TempDir at the given location.
   * @param target   path to the target
   * @param location relative path to the location within the TempDir
   */
  void MakeSymlink(const std::string &target, const std::string &location);

  /**
   * Sets additional files to be deleted from the TempDir on destruction
   * @param filesToClean list of filepaths relative to the TempDir
   */
  void SetCleanup(const std::vector<std::string> &filesToClean);

  /**
   * Do not clean the specified file.
   * Call this for easier debugging if an error occurs.
   * @param file filepath relative to the TempDir
   */
  void DontClean(const std::string &file);

  /**
   * @return the path to the TempDir
   */
  inline std::string Path() const noexcept { return path_; }

  /// Default move constructor
  TempDir(TempDir &&) = default;
  /// Default move assignment
  TempDir &operator=(TempDir &&) = default;

  /// Copy construction disabled, this owns the directory
  TempDir(const TempDir &) = delete;
  /// Copy assignment disabled, this owns the directory
  TempDir &operator=(const TempDir &) = delete;

  /**
   * Deletes all created symlinks and any files specified by SetCleanup.  Then
   * removes all directories created by this object if they are empty (in FIFO
   * order).
   */
  ~TempDir();

private:
  std::string path_;
  std::vector<std::string> filesToClean_;
  std::vector<std::string> dirsToClean_;

  static std::atomic<size_t> count_;
};

} // namespace Utilities
} // namespace EVADE
